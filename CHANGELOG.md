# quickom_contact_center_sdk_android changelog

## 1.0.19 (5/11/2021)
- allow to set desired language, if not set, device language will be used
- support display file message (pdf, doc, xls, xlsx, ppt, mp4)
- update README.md

## 1.0.20 (10/11/2021)
- Fix bug open file in release mode

## 1.0.24 (5/31/2022)
- fix some bugs and improve performance
- add function listen call
- integrate push message
- update README.md

## 1.0.25 (7/14/2022)
- fix some bugs and improve performance
- update README.md

## 1.0.26 (7/14/2022)
- fix some bugs and improve performance
- update README.md

## 1.1.1 (7/26/2022)
- fix some bugs and improve performance
- remove deviceId in push function
- update README.md

## 1.1.2 (7/27/2022)
- fix some bugs and improve performance
- update README.md