/**
 * Created by Nguyen Thanh Cong on 5/25/2022.
 * Beowulf company.
 * Copyright © 2020 Beowulf Inc. All rights reserved.
 */
package com.beowulfchain.quickom.callcentersdkexample

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class LauncherActivity : AppCompatActivity() {
    private val sharedPreferenceStorage = SharedPreferenceStorage(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (sharedPreferenceStorage.userName.isNullOrBlank()) {
            startActivity(Intent(this, LoginActivity::class.java))
        }
        else {
            startActivity(Intent(this, MainActivity::class.java))
        }
        finish()
    }
}