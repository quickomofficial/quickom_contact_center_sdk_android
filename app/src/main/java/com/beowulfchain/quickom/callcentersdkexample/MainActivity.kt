package com.beowulfchain.quickom.callcentersdkexample

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.beowulfchain.quickom.callcentersdk.CallCenterSdk
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.messaging.FirebaseMessaging
import timber.log.Timber
import java.util.*

class MainActivity : AppCompatActivity() {
    private val sharedPreferenceStorage = SharedPreferenceStorage(this)

    private var loadingDialog: AlertDialog? = null

    private val listener = object: CallCenterSdk.CallCenterSdkListener {
        override fun onSuccess() {
            loadingDialog?.dismiss()
        }

        override fun onError(errorCode: Int, errorDescription: String) {
            loadingDialog?.dismiss()
            runOnUiThread {
                showError(errorDescription)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var qrCodeAlias = ""
        var name = ""
        var email = ""
        var phoneNumber = ""

        val editTextQrCodeAlias = findViewById<EditText>(R.id.et_qr_code_alias)
        editTextQrCodeAlias.setText(qrCodeAlias)
        editTextQrCodeAlias.onChange {
            qrCodeAlias = it
        }

        val editTextName = findViewById<EditText>(R.id.et_name)
        editTextName.onChange {
            name = it
        }

        val editTextEmail = findViewById<EditText>(R.id.et_email)
        editTextEmail.onChange {
            email = it
        }

        val editTextPhoneNumber = findViewById<EditText>(R.id.et_phone_number)
        editTextPhoneNumber.onChange {
            phoneNumber = it
        }

        val buttonCallNormal = findViewById<Button>(R.id.bt_normal_call)
        buttonCallNormal.setOnClickListener {
            showLoading()
            CallCenterSdk.start(qrCodeAlias, name, email, phoneNumber , listener, CallCenterSdk.CallOption.NORMAL.value)
        }

        val buttonCall = findViewById<Button>(R.id.bt_call)
        buttonCall.setOnClickListener {
            showLoading()
            CallCenterSdk.start(qrCodeAlias, name, email, phoneNumber , listener, CallCenterSdk.CallOption.CALL.value)
        }

        val buttonChat = findViewById<Button>(R.id.bt_chat)
        buttonChat.setOnClickListener {
            showLoading()
            CallCenterSdk.start(qrCodeAlias, name, email, phoneNumber , listener, CallCenterSdk.CallOption.CHAT.value)
        }

        showLoading()
        CallCenterSdk.login(sharedPreferenceStorage.userName, sharedPreferenceStorage.password, object: CallCenterSdk.CallCenterSdkListener {
            override fun onSuccess() {
                loadingDialog?.dismiss()

                FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Timber.d("Fetching FCM registration token failed ${task.exception}")
                        return@addOnCompleteListener
                    }
                    Timber.d("Push token: ${task.result}")
                    CallCenterSdk.updatePushToken("quickom_sdk_app", task.result)
                }
            }

            override fun onError(errorCode: Int, errorDescription: String) {
                loadingDialog?.dismiss()
                runOnUiThread {
                    showError(errorDescription)
                }
            }
        })

        val buttonLogOut = findViewById<Button>(R.id.bt_log_out)
        buttonLogOut.setOnClickListener {
            CallCenterSdk.logOut()
            sharedPreferenceStorage.clearData()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    fun showLoading() {
        loadingDialog = MaterialAlertDialogBuilder(this)
            .setMessage("Loading")
            .create()
        loadingDialog?.show()
    }

    fun showError(error: String) {
        MaterialAlertDialogBuilder(this)
            .setTitle("Message")
            .setMessage(error)
            .setPositiveButton("Ok", null)
            .create()
            .show()
    }
}