/**
 * Created by Nguyen Thanh Cong on 8/10/2021.
 * Beowulf company.
 * Copyright © 2020 Beowulf Inc. All rights reserved.
 */
package com.beowulfchain.quickom.callcentersdkexample

import android.app.Application
import com.beowulfchain.quickom.callcentersdk.CallCenterSdk
import timber.log.Timber

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        CallCenterSdk.init(this, "com.beowulfchain.quickom.callcentersdkexample.fileprovider", "en")
    }
}