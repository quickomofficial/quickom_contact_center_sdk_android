/**
 * Created by Nguyen Thanh Cong on 5/23/2022.
 * Beowulf company.
 * Copyright © 2020 Beowulf Inc. All rights reserved.
 */
package com.beowulfchain.quickom.callcentersdkexample

import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.WorkerThread
import androidx.core.content.edit
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class SharedPreferenceStorage(
    private val context: Context
) {
    private val prefs: Lazy<SharedPreferences> = lazy {
        context.applicationContext.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE)
    }

    var userName: String? by StringPreference(prefs, USER_NAME, "")

    var password: String? by StringPreference(prefs, PASSWORD, "")

    fun clearData() {
        prefs.value.edit().clear().apply()
    }

    companion object {
        private const val APP_PREF = "APP_PREF"
        private const val USER_NAME = "USER_NAME"
        private const val PASSWORD = "PASSWORD"
    }
}

class StringPreference(
    private val preferences: Lazy<SharedPreferences>,
    private val name: String,
    private val defaultValue: String?
) : ReadWriteProperty<Any, String?> {

    @WorkerThread
    override fun getValue(thisRef: Any, property: KProperty<*>): String? {
        return preferences.value.getString(name, defaultValue)
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: String?) {
        preferences.value.edit { putString(name, value) }
    }
}