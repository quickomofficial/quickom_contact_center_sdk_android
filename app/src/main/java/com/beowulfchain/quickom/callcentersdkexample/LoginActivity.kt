/**
 * Created by Nguyen Thanh Cong on 5/25/2022.
 * Beowulf company.
 * Copyright © 2020 Beowulf Inc. All rights reserved.
 */
package com.beowulfchain.quickom.callcentersdkexample

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.beowulfchain.quickom.callcentersdk.CallCenterSdk
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class LoginActivity: AppCompatActivity() {
    private val sharedPreferenceStorage = SharedPreferenceStorage(this)

    private var loadingDialog: AlertDialog? = null

    private val listener = object: CallCenterSdk.CallCenterSdkListener {
        override fun onSuccess() {
            loadingDialog?.dismiss()
        }

        override fun onError(errorCode: Int, errorDescription: String) {
            loadingDialog?.dismiss()
            runOnUiThread {
                showError(errorDescription)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        var qrCodeAlias = ""
        var name = ""
        var email = ""
        var phoneNumber = ""

        val editTextQrCodeAlias = findViewById<EditText>(R.id.et_qr_code_alias)
        editTextQrCodeAlias.setText(qrCodeAlias)
        editTextQrCodeAlias.onChange {
            qrCodeAlias = it
        }

        val editTextName = findViewById<EditText>(R.id.et_name)
        editTextName.onChange {
            name = it
        }

        val editTextEmail = findViewById<EditText>(R.id.et_email)
        editTextEmail.onChange {
            email = it
        }

        val editTextPhoneNumber = findViewById<EditText>(R.id.et_phone_number)
        editTextPhoneNumber.onChange {
            phoneNumber = it
        }

        val buttonCallNormal = findViewById<Button>(R.id.bt_normal_call)
        buttonCallNormal.setOnClickListener {
            showLoading()
            CallCenterSdk.start(qrCodeAlias, name, email, phoneNumber , listener, CallCenterSdk.CallOption.NORMAL.value)
        }

        val buttonCall = findViewById<Button>(R.id.bt_call)
        buttonCall.setOnClickListener {
            showLoading()
            CallCenterSdk.start(qrCodeAlias, name, email, phoneNumber , listener, CallCenterSdk.CallOption.CALL.value)
        }

        val buttonChat = findViewById<Button>(R.id.bt_chat)
        buttonChat.setOnClickListener {
            showLoading()
            CallCenterSdk.start(qrCodeAlias, name, email, phoneNumber , listener, CallCenterSdk.CallOption.CHAT.value)
        }

        var userName = ""
        var password = ""

        val editTextUserName = findViewById<EditText>(R.id.et_user_name)
        editTextUserName.onChange {
            userName = it
        }

        val editTextPassword = findViewById<EditText>(R.id.et_password)
        editTextPassword.onChange {
            password = it
        }

        val buttonLogin = findViewById<Button>(R.id.bt_login)
        buttonLogin.setOnClickListener {
            showLoading()
            CallCenterSdk.login(userName, password, object: CallCenterSdk.CallCenterSdkListener {
                override fun onSuccess() {
                    loadingDialog?.dismiss()

                    sharedPreferenceStorage.userName = userName
                    sharedPreferenceStorage.password = password

                    startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                    finish()
                }

                override fun onError(errorCode: Int, errorDescription: String) {
                    loadingDialog?.dismiss()
                    runOnUiThread {
                        showError(errorDescription)
                    }
                }
            })
        }
    }

    fun showLoading() {
        loadingDialog = MaterialAlertDialogBuilder(this)
            .setMessage("Loading")
            .create()
        loadingDialog?.show()
    }

    fun showError(error: String) {
        MaterialAlertDialogBuilder(this)
            .setTitle("Message")
            .setMessage(error)
            .setPositiveButton("Ok", null)
            .create()
            .show()
    }
}