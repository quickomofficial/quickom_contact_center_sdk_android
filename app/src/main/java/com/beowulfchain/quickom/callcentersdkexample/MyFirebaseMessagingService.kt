/**
 * Created by Nguyen Thanh Cong on 6/14/2021.
 * Beowulf company.
 * Copyright © 2020 Beowulf Inc. All rights reserved.
 */
package com.beowulfchain.quickom.callcentersdkexample

import com.beowulfchain.quickom.callcentersdk.CallCenterSdk
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import timber.log.Timber

class MyFirebaseMessagingService : FirebaseMessagingService() {
    override fun onNewToken(token: String) {
        super.onNewToken(token)
        CallCenterSdk.updatePushToken("quickom_sdk_app", token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        val data = remoteMessage.data
        Timber.d("onMessageReceived: $data")
        CallCenterSdk.handlePushMessage(data)
    }
}