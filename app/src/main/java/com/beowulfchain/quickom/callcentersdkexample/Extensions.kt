/**
 * Created by Nguyen Thanh Cong on 5/25/2022.
 * Beowulf company.
 * Copyright © 2020 Beowulf Inc. All rights reserved.
 */
package com.beowulfchain.quickom.callcentersdkexample

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

fun EditText.onChange(input: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            input(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    })
}