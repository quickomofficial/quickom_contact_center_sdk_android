# Call Center Sdk Example

  This Android project showcases the steps to install and use call center sdk library.

  Latest version 1.1.2

------------
## Requirement
- minsdkversion: 24
- kotlin version: 1.6.10 above

### Step 1: Update build.gradle inside the application module
- Add the following code to **build.gradle** (or **settings.gradle**) inside the application module that will be using the library published on GitHub Packages Repository
```gradle
    repositories {
        maven {
            name = "GitHubPackages"
            url = uri("https://maven.pkg.github.com/quickomofficial/quickom_contact_center_sdk_android")

            credentials {
                username = "quickomofficial"
                password = "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"
            }
        }
        maven { url "https://jitpack.io" }
    }
```

### Step 2: Enable data binding and add library
- Enable data binding in build.gradle of app module
```gradle
    plugins {
        ...
        id 'kotlin-kapt'
    }
```
```gradle
android {
    ...
    buildFeatures {
        dataBinding true
    }
}
```

- Inside dependencies of the build.gradle of app module, use the following code
```gradle
    dependencies {
        implementation 'com.beowulfchain.quickom:callcentersdk:${latest_version}'
        ...
    }
```

## Setup
- Create your file provider follow this link [link create file provider](https://developer.android.com/reference/androidx/core/content/FileProvider)
- In your application file add the following code in function onCreate()
```kotlin
    class YourApplication : Application() {
        override fun onCreate() {
            super.onCreate()
            
            // Init call center sdk
            CallCenterSdk.init(this, "Your file provider authorities", "Your language (only two letter with lowercase)")
        }
    }
```
* Note: Call Center Sdk right now only support for two languages: english (en) and vietnamese (vi). Default language: vietnamese

- If you enable proguard for your project then add those lines in your app's proguard file
```text
-dontoptimize
-dontshrink

-keep class kotlin.** { *; }
-keep class kotlinx.** { *; }
-keep class kotlin.Metadata { *; }
-dontwarn kotlin.**
-dontwarn kotlinx.**
-keepclassmembers class **$WhenMappings {
    <fields>;
}
-keepclassmembers class kotlin.Metadata {
    public <methods>;
}
-assumenosideeffects class kotlin.jvm.internal.Intrinsics {
    static void checkParameterIsNotNull(java.lang.Object, java.lang.String);
}

-dontwarn androidx.databinding.**
-keep class androidx.databinding.** { *; }
-keep class * extends androidx.databinding.DataBinderMapper

-keep class org.jivesoftware.** { *; }

-keep class org.webrtc.** { *; }

-keep class org.xmlpull.** { *; }
-dontwarn org.xmlpull.v1.**

-keepclassmembers class com.beowulfchain.quickom.callcentersdk.model.* { *; }
```

## Events
### CallCenterSdkListener
- This interface is a callback of all events return to your app from call center sdk every time you call function of call center sdk
```kotlin
    val listener = object: CallCenterSdk.CallCenterSdkListener {
        override fun onSuccess() {
            // return success for your function call
        }

        override fun onError(errorCode: Int, errorDescription: String) {
            // return error for your function call
        }
    }
```

- ErrorCode

    | Code                  | Description                                                 |
    |-----------------------|-------------------------------------------------------------|
    | 10100                 | Url is invalid                                              |
    | 10101                 | Id is invalid                                               |
    | 10102                 | Email is invalid                                            |
    | 10103                 | Phone number is invalid                                     |
    | 20101                 | Qr code not allow call                                      |
    | 20102                 | Qr code not allow chat                                      |
    | 30100                 | Account is not activated                                    |
    | 30101                 | Account not found                                           |
    | 30102                 | Wrong user name or password                                  |
    | -1                    | Error unknown                                               |


## Usage
### Start a call
- Add the following code every time you want to start a call
```kotlin
    /**
    * Function to start a call
    * @param qrCode: alias of qr code
    * @param name: caller's name
    * @param email: caller's email
    * @param phoneNumber: caller's phone number
    */
    fun startCall(qrCode: String?, name: String?, email: String?, phoneNumber: String?, listener: CallCenterSdkListener, callOption: Int) {
        CallCenterSdk.start(qrCode, name, email, phoneNumber, listener, callOption)
    }
```


- Call Option:

    | Option                                      | Description                                                                              |
    |---------------------------------------------|------------------------------------------------------------------------------------------|
    | CallOption.NORMAL(value : 0)                | Start a normal call that means you need to pass one more screen to actually start a call |
    | CallOption.CALL(value : 1)                  | Go straight to start a call                                                              |
    | CallOption.CHAT(value : 2)                  | Go straight to start a chat                                                              |

### Login to listen call
- Add the following code every time you login or open app (if already login)
```kotlin
    /**
    * Function to login
    * @param userName: account's user name
    * @param password: account's password
    */
    fun login(userName: String?, password: String?, listener: CallCenterSdkListener) {
        CallCenterSdk.login(userName, password, listener)
    }
```

### Update push token
- Add the following code every time your firebase token renew or everytime you enter in app
```kotlin
    /**
    * Function to update push token
    * @param enterpriseName: your enterprise name
    * @param pushToken: firebase's push token
    */
    fun updatePushToken(enterpriseName: String?, pushToken: String?) {
        CallCenterSdk.updatePushToken(enterpriseName, pushToken)
    }
```
* Note: you need to setup your own firebase push message on your android client (link: https://firebase.google.com/docs/cloud-messaging/android/client).
After finishing setup you need to give us your server key of your firebase cloud message in order to config on our server.

### Log out
- Add the following code every time you log out app
```kotlin
    /**
    * Function to log out
    */
    fun logOut() {
        CallCenterSdk.logOut()
    }
```

### Handle push message
- Add the following code every time you receive message push from firebase
```kotlin
    /**
    * Function to handle push message
    * @param data: data inside message push
    */
    fun handlePushMessage(data: Map<String, String>) {
        CallCenterSdk.handlePushMessage(data)
    }
```